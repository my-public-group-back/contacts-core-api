#!/bin/bash
registry=registry.gitlab.com/my-public-group-back/contacts-core-api
tag=DEV

# Docker build and docker push to registry
docker build -f docker/Dockerfile . -t $registry:"$tag" && docker push $registry:"$tag"
