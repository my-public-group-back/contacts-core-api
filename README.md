## Description

[Contacts Core API](https://gitlab.com/my-public-group-back/contacts-core-api) Contacts Core API repository.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run docker:mysql
$ npm run start:dev

# production mode
$ npm run docker:start
```

## Test

```bash
# unit tests
$ npm run test
```

## URL's Production in Heroku

- [Home Endpoint](https://contacts-core-api.herokuapp.com)
- [Swagger API Documentation](https://contacts-core-api.herokuapp.com/swagger)
- [App Registry](registry.gitlab.com/my-public-group-back/contacts-core-api:DEV)

## Data Model

[DataModel](https://i.imgur.com/xtS98y6.png)

## Support

This project is an MIT-licensed open source project.

## Stay in touch

- Author - [Jose Lorenzo Moreno Moreno](https://www.linkedin.com/in/jolorenzom/)
- Contact - [Email](jolorenzom@gmail.com)

## License

  Contacts Core Api is [MIT licensed](https://gitlab.com/my-public-group-back/contacts-core-api/-/blob/master/LICENSE).
