import { NestFactory } from '@nestjs/core';
import { AppModule } from './app/app.module';
import { appModuleConfig } from './app/app.config';
import * as morgan from 'morgan';
import * as cors from 'cors';
import { Logger, ValidationPipe } from '@nestjs/common';
import { TransformInterceptor } from './app/shared/interceptors/transform.interceptor';
import * as dotenv from 'dotenv';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  // Load Logger
  const logger = new Logger('main');

  // Load ENVs
  dotenv.config();

  const app = await NestFactory.create(AppModule, {
    logger: ['log', 'error', 'warn', 'debug', 'verbose'],
  });

  // Log every request to the console
  app.use(morgan(appModuleConfig.morgan));

  // Enable CORS
  app.use(cors(appModuleConfig.cors));

  // Validate inputs with our DTOs
  app.useGlobalPipes(new ValidationPipe());

  // Custom transform response
  app.useGlobalInterceptors(new TransformInterceptor());

  const options = new DocumentBuilder()
    .setTitle('Contacts Core API')
    .setDescription('The Contacts Core API description')
    .setVersion('1.0')
    .addTag('Contacts Core API')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('swagger', app, document);

  await app.listen(process.env.PORT);

  logger.log(
    `\r\nProject name: ${process.env.PROJECT_NAME}
    Url: ${process.env.PROTOCOL}://${process.env.URL}:${process.env.PORT},
    Swagger Documentation: ${process.env.PROTOCOL}://${process.env.URL}:${process.env.PORT}/swagger,
    PID: ${process.pid}
    Enviroment: ${process.env.NODE_ENV}
    Root: ${process.env.PWD}`,
  );
}
bootstrap();
