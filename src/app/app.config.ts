export const appModuleConfig = {
  morgan: 'dev',
  cors: {
    origin: true,
    methods: ['GET', 'PATCH', 'PUT', 'POST', 'DELETE'],
    allowedHeaders: [
      'Content-Type',
      'Authorization',
      'Content-Length',
      'Content-Language',
    ],
    exposedHeaders: ['x-provider', 'limit', 'page', 'count', 'X-Response-Time'],
  },
  neutrinoApiUrl: 'https://neutrinoapi.net/phone-validate',
};
