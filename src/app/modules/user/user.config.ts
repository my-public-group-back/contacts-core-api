export const userModuleConfig = {
  api: {
    route: 'users',
    param: 'id',
  },
  entities: {
    model: 'users',
  },
};
