import { Body, Controller, Post } from '@nestjs/common';
import { UserService } from './user.service';
import { UserEntity } from './user.entity';
import { CreateUserDto } from './create.user.dto';
import { userModuleConfig } from './user.config';
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';

@ApiTags(userModuleConfig.api.route)
@Controller(userModuleConfig.api.route)
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post()
  @ApiOperation({ summary: 'Create an user' })
  @ApiCreatedResponse({
    description: 'Created',
    status: 201,
    type: UserEntity,
  })
  @ApiBadRequestResponse({
    description: 'user-with-this-phone-already-exists',
    status: 400,
  })
  @ApiForbiddenResponse({
    description: 'invaid-phone-format',
    status: 403,
  })
  async createUser(@Body() createUserDto: CreateUserDto): Promise<UserEntity> {
    await this.userService.checkUserPhone(createUserDto.phone);
    return await this.userService.create(createUserDto);
  }
}
