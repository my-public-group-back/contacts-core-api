import { userModuleConfig } from './user.config';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

@Entity(userModuleConfig.entities.model)
export class UserEntity {
  @PrimaryGeneratedColumn()
  @ApiProperty()
  id: number;

  @Column({ type: 'varchar', unique: false, nullable: false })
  @ApiProperty()
  name: string;

  @Column({ type: 'varchar', unique: false, nullable: false })
  @ApiProperty()
  lastName: string;

  @Column({ type: 'varchar', unique: true, nullable: false })
  @ApiProperty()
  phone: string;
}
