import { Test, TestingModule } from '@nestjs/testing';
import { UserService } from './user.service';
import { UserEntity } from './user.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { BadRequestException, HttpModule } from '@nestjs/common';
import * as dotenv from 'dotenv';

// Load ENVs
dotenv.config();

describe('Test userService methods', () => {
  let userService: UserService;
  const oneUser = new UserEntity();

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [
        UserService,
        {
          provide: getRepositoryToken(UserEntity),
          useValue: {
            checkUserPhone: jest
              .fn()
              .mockResolvedValue(
                new BadRequestException('user-with-this-phone-already-exists'),
              ),
            checkPhoneFormat: jest.fn().mockResolvedValue(true),
            findOne: jest.fn().mockResolvedValue(oneUser),
          },
        },
      ],
      //exports: [process.env.NEUTRINO_API_KEY, process.env.NEUTRINO_API_ID]
    }).compile();

    userService = module.get<UserService>(UserService);
  });

  describe('findOne', () => {
    it('should return one User', async () => {
      const result = await userService.findOne({ id: '1' });
      expect(result).toEqual(oneUser);
    });
  });

  describe('checkPhoneFormat OK', () => {
    it('should return true', async () => {
      const result = await userService.checkPhoneFormat('+34612000001');
      expect(result).toBe(true);
    });
  });

  describe('checkPhoneFormat KO', () => {
    it('should return false', async () => {
      const result = await userService.checkPhoneFormat('+34000000001');
      expect(result).toBe(false);
    });
  });

  describe('checkUserPhone', () => {
    it('should return Bad Request Exception', async () => {
      try {
        await userService.checkUserPhone('+34612000000');
      } catch (e) {
        expect(e).toBeInstanceOf(BadRequestException);
        expect(e.message).toBe('user-with-this-phone-already-exists');
      }
    });
  });
});
