import {
  BadRequestException,
  ForbiddenException,
  HttpService,
  Injectable,
} from '@nestjs/common';
import { UserEntity } from './user.entity';
import { AbstractMysqlService } from '../../shared/services/abstract.mysql.service';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { appModuleConfig } from '../../app.config';

@Injectable()
export class UserService extends AbstractMysqlService<UserEntity> {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    private readonly httpService: HttpService,
  ) {
    super(userRepository);
  }

  async checkUserPhone(phone: string) {
    const user = await this.findOne({
      phone,
    });
    if (user) {
      throw new BadRequestException('user-with-this-phone-already-exists');
    }
    // Check if phone number is valid (https://www.neutrinoapi.com/api/phone-validate/)
    const result = await this.checkPhoneFormat(phone);
    if (!result) {
      throw new ForbiddenException('invaid-phone-format');
    }
  }

  async checkPhoneFormat(phone) {
    return new Promise((resolve, reject) => {
      this.httpService
        .post(
          appModuleConfig.neutrinoApiUrl,
          { number: phone },
          {
            params: {
              'user-id': process.env.NEUTRINO_API_ID,
              'api-key': process.env.NEUTRINO_API_KEY,
            },
          },
        )
        .subscribe(
          (response: any) => {
            resolve(response.data.valid);
          },
          (error) => {
            reject(error.message);
          },
        );
    });
  }
}
