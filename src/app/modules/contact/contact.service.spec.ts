import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ContactEntity } from './contact.entity';
import { ContactService } from './contact.service';
import { UserEntity } from '../user/user.entity';

const testContact1 = 'Test Cat 1';
const testContact2 = 'Test Cat 1';

const contactsArray = [new ContactEntity(), new ContactEntity()];

const oneContact = new ContactEntity();
const user = new UserEntity();

describe('Test contactService methods', () => {
  let service: ContactService;
  let repo: Repository<ContactEntity>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ContactService,
        {
          provide: getRepositoryToken(ContactEntity),
          useValue: {
            findAllCommonContacts: jest.fn().mockResolvedValue(contactsArray),
            createContacts: jest.fn().mockResolvedValue(contactsArray),
            find: jest.fn().mockResolvedValue(contactsArray),
            update: jest.fn().mockResolvedValue(oneContact),
            findOne: jest.fn().mockResolvedValue(oneContact),
            checkIfUserContactExists: jest
              .fn()
              .mockResolvedValue(contactsArray),
          },
        },
      ],
    }).compile();

    service = module.get<ContactService>(ContactService);
    repo = module.get<Repository<ContactEntity>>(
      getRepositoryToken(ContactEntity),
    );
  });

  describe('findAllCommonContacts', () => {
    it('should return an array of contacts', async () => {
      const contacts = await service.findAllCommonContacts({
        userId1: '1',
        userId2: '2',
      });
      expect(contacts).toEqual(contactsArray);
    });
  });

  describe('createContacts', () => {
    it('should successfully insert an array of contacts', () => {
      expect(
        service.createContacts(
          [
            {
              contactName: 'contactName test',
              phone: '0987654321',
              user: undefined,
            },
            {
              contactName: 'contactName test2',
              phone: '1234567890',
              user: undefined,
            },
          ],
          user,
        ),
      ).resolves.toEqual(contactsArray);
    });
  });

  describe('checkIfUserContactExists', () => {
    it('should get a single cat', async () => {
      const contacts = await service.checkIfUserContactExists(
        '1234567890',
        user,
      );
      expect(contacts).toEqual(contactsArray);
    });
  });
});
