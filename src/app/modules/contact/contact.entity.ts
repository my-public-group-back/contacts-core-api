import { contactModuleConfig } from './contact.config';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { UserEntity } from '../user/user.entity';
import { ApiProperty } from '@nestjs/swagger';

@Entity(contactModuleConfig.entities.model)
export class ContactEntity {
  @PrimaryGeneratedColumn()
  @ApiProperty()
  id: number;

  @Column({ type: 'varchar', unique: false, nullable: true, default: 'empty' })
  @ApiProperty()
  contactName: string;

  @Column({ type: 'varchar', unique: false, nullable: false })
  @ApiProperty()
  phone: string;

  @ManyToOne((type) => UserEntity)
  @ApiProperty()
  user: UserEntity;
}
