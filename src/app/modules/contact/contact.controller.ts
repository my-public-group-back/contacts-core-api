import {
  BadRequestException,
  Body,
  Controller,
  Get,
  NotFoundException,
  Post,
  Query,
} from '@nestjs/common';
import { ContactService } from './contact.service';
import { ContactEntity } from './contact.entity';
import { CreateContactDto } from './create.contact.dto';
import { contactModuleConfig } from './contact.config';
import { UserService } from '../user/user.service';
import {
  ApiBadRequestResponse,
  ApiBody,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOperation,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';

@ApiTags(contactModuleConfig.api.route)
@Controller(contactModuleConfig.api.route)
export class ContactController {
  constructor(
    private readonly contactService: ContactService,
    private readonly userService: UserService,
  ) {}

  @Post()
  @ApiOperation({ summary: 'Create or update an user`s contacts' })
  @ApiCreatedResponse({
    description: 'Created',
    status: 201,
    type: ContactEntity,
    isArray: true,
  })
  @ApiBadRequestResponse({
    description: 'user-query-param-is-required',
    status: 400,
  })
  @ApiNotFoundResponse({
    description: 'user-not-found',
    status: 404,
  })
  @ApiQuery({
    name: 'user',
    description: 'User id to create contacts',
    required: true,
    type: String,
  })
  @ApiBody({
    type: CreateContactDto,
    description: 'Create Contact Dto',
    isArray: true,
  })
  async createContact(
    @Query() queryParams,
    @Body() createContactsDto: CreateContactDto[],
  ): Promise<ContactEntity[]> {
    if (!queryParams.user) {
      throw new BadRequestException('user-query-param-is-required');
    }
    const user = await this.userService.findOne({ id: queryParams.user });
    if (!user) {
      throw new NotFoundException('user-not-found');
    }
    // Note: In this project, is not allowed to delete a user's contact,
    // Only create a contact and update the contactName.
    return await this.contactService.createContacts(createContactsDto, user);
  }

  @Get()
  @ApiOperation({ summary: 'Get an user`s contacts' })
  @ApiResponse({
    description: 'OK',
    status: 200,
    type: ContactEntity,
    isArray: true,
  })
  @ApiBadRequestResponse({
    description: 'user-query-param-is-required',
    status: 400,
  })
  @ApiQuery({
    name: 'user',
    description: 'User id to get his contacts',
    required: true,
    type: String,
  })
  async findByUserContacts(@Query() queryParams): Promise<ContactEntity[]> {
    if (!queryParams.user) {
      throw new BadRequestException('user-query-param-is-required');
    }
    return await this.contactService.findAll({ user: queryParams.user });
  }

  @Get('/common')
  @ApiOperation({ summary: 'Get a common contacts of two users' })
  @ApiResponse({
    description: 'OK',
    status: 200,
    type: ContactEntity,
    isArray: true,
  })
  @ApiBadRequestResponse({
    description: 'queryParams-userId1-and-userId2-are-required',
    status: 400,
  })
  @ApiQuery({
    name: 'userId1',
    description: 'First User id to get common contacts',
    required: true,
    type: String,
  })
  @ApiQuery({
    name: 'userId2',
    description: 'Second User id to get common contacts',
    required: true,
    type: String,
  })
  async findCommonContacts(@Query() queryParams): Promise<ContactEntity[]> {
    if (!(queryParams.userId1 && queryParams.userId2)) {
      throw new NotFoundException(
        'queryParams-userId1-and-userId2-are-required',
      );
    }
    return await this.contactService.findAllCommonContacts(queryParams);
  }
}
