import { IsNotEmpty, IsString } from 'class-validator';
import { UserEntity } from '../user/user.entity';
import { ApiProperty } from '@nestjs/swagger';

export class CreateContactDto {
  @IsString()
  @ApiProperty()
  contactName: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  phone: string;

  @IsNotEmpty()
  @ApiProperty()
  user: UserEntity;
}
