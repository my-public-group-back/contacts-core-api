import { IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UpdateContactDto {
  @IsString()
  @ApiProperty()
  contactName: string;
}
