import { Injectable } from '@nestjs/common';
import { ContactEntity } from './contact.entity';
import { AbstractMysqlService } from '../../shared/services/abstract.mysql.service';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateContactDto } from './create.contact.dto';
import { UserEntity } from '../user/user.entity';
import { UpdateContactDto } from './update.contact.dto';

@Injectable()
export class ContactService extends AbstractMysqlService<ContactEntity> {
  constructor(
    @InjectRepository(ContactEntity)
    private readonly contactRepository: Repository<ContactEntity>,
  ) {
    super(contactRepository);
  }

  async findAllCommonContacts(params) {
    const contactsOfUser1AndUser2 = await this.findAll([
      { user: params.userId1 },
      { user: params.userId2 },
    ]);
    const commonContacts: ContactEntity[] = [];
    for (const userContact of contactsOfUser1AndUser2) {
      const common = contactsOfUser1AndUser2.filter(
        (contact) => contact.phone === userContact.phone,
      ); // Is common when phone contact of user 1 is equal to phone contact user 2
      if (common.length > 1) {
        // Repeated phone. More than 1
        commonContacts.push(userContact);
      }
    }
    return commonContacts;
  }

  async createContacts(
    createContactsDto: CreateContactDto[],
    user: UserEntity,
  ) {
    const createdContacts: ContactEntity[] = [];
    for (const contactDto of createContactsDto) {
      const contact = await this.checkIfUserContactExists(
        contactDto.phone,
        user,
      );
      if (contact.length === 0) {
        // Non repeated User-Phone -> save Contact
        contactDto.user = user;
        const contactCreated = await this.create(contactDto);
        createdContacts.push(contactCreated);
      } else {
        // Update contactName ONLY!!!
        const updateContactDto: UpdateContactDto = {
          contactName: contactDto.contactName,
        };
        const contactUpdated = await this.update(
          contact[0].id,
          updateContactDto,
        );
        createdContacts.push(contactUpdated);
      }
    }
    return createdContacts;
  }

  async checkIfUserContactExists(phone: string, user: UserEntity) {
    return await this.findAll({ user: user.id, phone: phone });
  }
}
