export const contactModuleConfig = {
  api: {
    route: 'contacts',
    param: 'id',
  },
  entities: {
    model: 'contacts',
  },
};
