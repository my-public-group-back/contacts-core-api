import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';

@Injectable()
export abstract class AbstractMysqlService<Entity> {
  protected constructor(private readonly repository: Repository<Entity>) {}

  async create(createDto): Promise<Entity> {
    return await this.repository.save(createDto);
  }

  async findOne(query: object): Promise<Entity> {
    return await this.repository.findOne({
      where: query,
    });
  }

  async findAll(query): Promise<Entity[]> {
    return await this.repository.find({
      where: query,
    });
  }

  async update(id, updateDto): Promise<Entity> {
    await this.repository.update(id, updateDto);
    return await this.findOne({ id });
  }
}
