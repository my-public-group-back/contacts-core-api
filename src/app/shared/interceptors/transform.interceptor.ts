import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { isArray } from 'lodash';

export interface Response<T> {
  data: T;
}

@Injectable()
export class TransformInterceptor<T>
  implements NestInterceptor<T, Response<T>> {
  intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Observable<Response<T>> {
    return next.handle().pipe(
      map((data) => {
        const response = context.switchToHttp().getRequest();
        if (data && isArray(data)) {
          if (!response.res) {
            return data;
          }
          if (!response.res.headers) {
            response.res.headers = {};
          }
          // Set headers
          response.res.set({ count: data.length });
        }
        return { data };
      }),
    );
  }
}
